<?php

namespace App\Controller;

use App\Entity\Patient;
use App\Form\PatientFilterType;
use App\Form\PatientListFormType;
use App\Form\PatientsListFormType;
use App\Form\RegistrationFormType;
use App\Form\UserEditFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PatientController
 * @package App\Controller
 * @IsGranted("ROLE_USER")
 */

class PatientController extends AbstractController
{
    /**
     * @Route("/patient/list", name="app_patients_list")
     */
    public function listAction()
    {
        $patients = $this->getDoctrine()->getRepository(Patient::class)->findAll();

        return $this->render('patient/index.html.twig', array(
            'patients' => $patients
        ));
    }

    /**
     * @Route("/patient/assign", name="app_patients_assign")
     */
    public function patientAssign(Request $request)
    {
        $form = $this->createForm(PatientFilterType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('app_users_list');
        }

        return $this->render('patient/assign.html.twig', array(
            'patientForm' => $form->createView()
        ));
    }

    public function getPatientByFilter(Request $request)
    {
        $form = $this->createForm(PatientFilterType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->createForm(PatientsListFormType::class);
        }
    }
}
