<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserDetailsFormType;
use App\Form\UserEditFormType;
use App\Form\UserFilterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 * @IsGranted("ROLE_USER")
 */

class UserController extends AbstractController
{
    /**
     * @Route("/", name="app_users_list", methods={"GET"})
     */
    public function userList()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('user/index.html.twig', array(
            'users' => $users
        ));
    }

    /**
     * @Route("user/edit/{id}", name="app_users_edit")
     */
    public function edit(Request $request, $id)
    {
        $user = new User();
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $form = $this->createForm(UserEditFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('app_users_list');
        }

        return $this->render('user/edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/user/details/{id}", name="app_user_details")
     */

    public function details($id)
    {
        $user = new User();
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $form = $this->createForm(UserDetailsFormType::class, $user);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('app_users_list');
        }

        return $this->render('user/edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/user/delete/{id}", name="app_user_delete")
     */

    public function delete(Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $response = new Response();
        $response->send();
    }

    /**
     * @Route("/user/departments", name="app_departments")
     */

    public function departments()
    {
        return $this->render('user/departments.html.twig');
    }
}
