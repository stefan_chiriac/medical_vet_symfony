<?php

namespace Symfony\Component\Form;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserFilterForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Registry $doctrine */
        $doctrine = $options['doctrine'];

        $builder->add(
            'userConfigurationRoles',
            ChoiceType::class,
            [
                'choices' => array_merge([-1], array_keys())
            ]
        );
    }
}
