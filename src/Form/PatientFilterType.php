<?php

namespace App\Form;

use App\Entity\Patient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PatientFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('race', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('type', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('Search', SubmitType::class, array(
                'label' => 'Search',
                'attr' => array(
                    'class' => 'btn btn-primary mt-3'
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Patient::class,
        ]);
    }
}
